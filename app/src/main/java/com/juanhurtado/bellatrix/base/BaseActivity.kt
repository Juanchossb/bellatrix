package com.juanhurtado.bellatrix.base

import android.content.Context
import android.os.Bundle
import android.os.PersistableBundle
import android.support.v7.app.AppCompatActivity
import com.juanhurtado.bellatrix.R

abstract class BaseActivity : AppCompatActivity(), BaseView{

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
        setContentView(R.layout.activity_base)
    }
    override fun getViewContext(): Context = applicationContext

}