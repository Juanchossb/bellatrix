package com.juanhurtado.bellatrix.base

import android.content.Context

interface BaseView{
    fun getViewContext() : Context
}