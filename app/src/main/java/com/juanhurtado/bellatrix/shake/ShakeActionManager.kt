package com.juanhurtado.bellatrix.shake

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.support.v4.content.LocalBroadcastManager

class ShakeActionManager constructor(val context : Context, val shakeListener : ShakeActionListener) : BroadcastReceiver() {

    init {
        context.startService(Intent(context, ShakeService::class.java))
        LocalBroadcastManager.getInstance(context).registerReceiver(this, IntentFilter("event"))
    }

    override fun onReceive(context: Context?, intent: Intent?) {
        shakeListener.onShakeEvent()
    }

    fun unregisterReceiver(){
        LocalBroadcastManager.getInstance(context).unregisterReceiver(this)
    }

}

interface ShakeActionListener{
    fun onShakeEvent()
}