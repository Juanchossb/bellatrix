package com.juanhurtado.bellatrix.shake

import android.app.IntentService
import android.content.Context
import android.content.Intent
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.support.v4.content.LocalBroadcastManager

class ShakeService : IntentService("service"), SensorEventListener {

    lateinit var sensorManager: SensorManager
    lateinit var broadcastManager: LocalBroadcastManager
    var sensor: Sensor? = null
    var mGravity: FloatArray? = null
    var accel = 0f
    var accelLast = 0f
    var accelNow = 0f
    var delta = 0f

    override fun onHandleIntent(intent: Intent?) {}

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {}

    override fun onSensorChanged(event: SensorEvent?) {

                val orientation = event?.values ?: FloatArray(3)
                accelLast = accelNow
                accelNow =
                    Math.sqrt((orientation[0] * orientation[0] + orientation[1] * orientation[1] + orientation[2] + orientation[2]).toDouble())
                        .toFloat()
                delta = accelNow - accelLast
                accel = accel * 0.9f + delta
                if (accel > 9) {
                    broadcastManager.sendBroadcast(Intent("event"))
                }
    }

    override fun onCreate() {
        super.onCreate()
        broadcastManager = LocalBroadcastManager.getInstance(this)
        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
        sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL)
    }

    override fun onDestroy() {
        super.onDestroy()
        sensorManager.unregisterListener(this, sensor)
    }

}