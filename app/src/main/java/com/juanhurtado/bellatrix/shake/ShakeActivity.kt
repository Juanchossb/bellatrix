package com.juanhurtado.bellatrix.shake

import android.os.Bundle
import android.os.Handler
import android.view.View
import com.juanhurtado.bellatrix.R
import com.juanhurtado.bellatrix.base.BaseActivity
import kotlinx.android.synthetic.main.activity_shake.*

class ShakeActivity : BaseActivity() ,ShakeActionListener{
    private lateinit var shakeActionManager : ShakeActionManager

    override fun onShakeEvent() {
        tv_shake.visibility = View.GONE
        Handler().postDelayed({tv_shake.visibility = View.GONE},3000)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shake)
    }

    override fun onResume() {
        super.onResume()
        shakeActionManager = ShakeActionManager(this,this)
    }


    override fun onStop() {
        super.onStop()
        shakeActionManager?.unregisterReceiver()
    }

}