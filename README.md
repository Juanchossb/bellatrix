# Shaker
Shaker action, detecs when device has been shaken. onShakeEvent will be called every time the device detects a shake action
# Installation
Clone repository
```sh
git clone https://Juanchossb@bitbucket.org/Juanchossb/bellatrix.git
```
# Implementation
Instantitate 
```kotlin 
ShakeActionManager(Context,ShakeActionListener) 
```
and override method 
```kotlin
onShakeEvent()
```